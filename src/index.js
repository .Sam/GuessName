require('dotenv').config()
const fs = require('fs');
const express = require('express')
const mongoose = require('mongoose')
const routes = require("./routes");
const populateDb = require("./controler/populateDb")
const app = express();
const port = 3000;

//connecting to db
const mdbUrl = "mongodb+srv://usr:123soleil@cluster0.pzgeq.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
mongoose.connect(mdbUrl,function(err)
{
	if (err)
		console.log(err);
	else
		console.log('🚀 Connection to database established')
})


//including usful files
app.use(express.static('public'))
app.use(express.static('views'))
app.use('/css',express.static(__dirname + 'public/css'))
app.use('/js',express.static(__dirname + 'public/js'))
app.use("/", routes);

//Reading file and populating DB
fs.readFile(__dirname +'/names.txt', 'utf8' , async(err, data) => {
		if (err) {
			console.error(err)
			return
		}
		if (data)
			populateDb(data)
})

//listening on specific port
app.listen(port, () => {
  console.log(`🚀 The app is listening on port ${port}`)
});
