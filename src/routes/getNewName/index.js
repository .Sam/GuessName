const names = require('../../model/name')
const axios = require('axios');

function getRandomInt(max) {
  return Math.floor(Math.random() * max);
}

const requestGender = async(name)=>{
  let ret = null
  await axios.get('https://gender-api.com/get?name='+name+'&key='+process.env.API_KEY)
    .then(function (res) {
      ret =  res.data.gender
    })
    .catch(function (error) {
      console.log(error);
    })
    return ret
}

const getNewName = async() =>{
  let nms = await names.find({})
  let randMax = nms.length
  let rand = getRandomInt(randMax)
  let chooseName = nms[rand]
  let gender = await requestGender(chooseName.name)
  return {
    "name":chooseName.name,
    "gender": gender
  }
}
module.exports = getNewName
