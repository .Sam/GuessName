const express = require("express");
const bodyParser = require('body-parser');
const router = express.Router();
const axios = require('axios');
const getNewName = require('./getNewName')

router.get("/", async (req, res) => {
  try{
    res.sendFile("index.html",{ root: "./src/views" })
  }catch(err){
      return res.status(500).json({
        status:500,
        message:"Internal error",
      })
    }
})

router.get("/getNewName", async (req, res) => {
  try{
  res.json(await getNewName(req, res))
  }catch(err){
    return res.status(500).json({
      status:500,
      message:"Internal error",
    })
}
})

module.exports = router;
