const names = require('../../model/name')

const registerNames = async(array) =>{
  let i = 0;
  let actualName = ""
  let splitedString = []
  let populatedNum = 0
  console.log("  Cheking new names that are not in the database for populating it ...")
  //Brosing the first 300 line
  while (i < array.length && i < 300){
    //getting he name
    splitedString = array[i].split(" ")
    actualName = splitedString[0]
    //check if name exist in the db to avoid duplicates
    let nameAlradyExist = await names.find({"name":actualName})
    if (nameAlradyExist.length === 0){
      let name = new names({
        "name":actualName
      })
      try{
        //saving name
        await name.save()
      }catch(err){
        console.log("ERROR :",err)
      }
      populatedNum = populatedNum + 1;
    }
    i++
  }
  console.log("🚀  Database populated with ", populatedNum, "name form the first 300 name in file names.txt which have not been found in database")
}

const populateDb = async(data)=>{
  let allNames = await names.find({})
    const lines = data.split(/\r?\n/);
    registerNames(lines)
}

module.exports = populateDb
